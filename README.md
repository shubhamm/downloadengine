# README #

Executable java jar, to be run as:

`java -jar downloader.jar http://www.7-zip.org/a/7z1604-x64.exe sftp://shubhamme:passwd@127.0.0.1/tmp/loremipsum.txt ftp://user:pass@localhost:2121/jdk-8u65-windows-x64.exe  /downloads`

Where first three are the URLs to be downloaded

The last argument is the target directory

Note the syntax for FTP/SFTP downloads: sftp://[username:password@]dowmainname/path/to/file

### What is this repository for? ###

Provides a console based download manager which runs 8 downloads in parallel

### How do I get set up? ###

Build with maven

Built on Java 7

### Contribution guidelines ###

Feel free to contribute

### Known Issues with the current build ###

On windows machine with java 7, there is an OS bug which causes issues in establishing FTP connections, refer: [java-7-prevents-ftp-transfers-on-windows](http://stackoverflow.com/questions/6990663/java-7-prevents-ftp-transfers-on-windows-vista-and-7-if-firewall-is-on-any-idea)

For now use the hotfix or FTP connect on some port other than 21

### Who do I connect with for further questions? ###

Shubham Mehrotra

[shubham.meh@gmail.com](mailto:shubham.meh@gmail.com)