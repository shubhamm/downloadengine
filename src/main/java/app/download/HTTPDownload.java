package app.download;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import framework.Download;
import framework.DownloadActions;

public class HTTPDownload extends Download {

	private CloseableHttpClient httpclient;
	
	private long contentLength;
	
	public HTTPDownload(URI source, File target, DownloadActions actions) {
		super(source, target, actions);
	}

	@Override
	public InputStream setupDownloadStream() throws IOException, ClientProtocolException {
		httpclient = HttpClients.createDefault();
		HttpGet httpGet = new HttpGet(source);
		CloseableHttpResponse response = httpclient.execute(httpGet);
		response.getStatusLine().getStatusCode();
		HttpEntity entity = response.getEntity();
		contentLength = entity.getContentLength();
		InputStream inputStream = entity.getContent();
		return inputStream;
	}
	
	@Override
	public long getTotalSize() {
		return contentLength;
	}
	
	@Override
	public void postDownloadCleanup() throws IOException {
		httpclient.close();
	}

	@Override
	protected String getFileName() {
		String path = source.getPath();
		return path.substring(path.lastIndexOf("/"));
	}

}
