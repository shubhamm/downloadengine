package app.download;

import java.io.File;
import java.io.InputStream;
import java.net.URI;

import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystem;
import org.apache.commons.vfs2.FileSystemManager;
import org.apache.commons.vfs2.VFS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import framework.Download;
import framework.DownloadActions;

public class SFTPDownload extends Download {

	private FileSystemManager fsManager = null;
	private FileObject sftpFile;
	private static final Logger logger = LoggerFactory.getLogger(SFTPDownload.class);
	
	private SFTPDownload() {
		super(null, null, null);
	}
	
	public SFTPDownload(URI source, File target, DownloadActions actions) {
		super(source, target, actions);
	}

	@Override
	public long getTotalSize() {
		try{
			return sftpFile.getContent().getSize();
		}
		catch(Exception ex) {
			logger.error("Unable to determine filesize for "+source.toASCIIString(), ex);
		}
		return getCurrentSize();
	}
	
	@Override
	protected String getFileName() {
		return sftpFile.getName().getBaseName();
	}

	@Override
	protected InputStream setupDownloadStream() throws Exception {
		fsManager = VFS.getManager();
		sftpFile = this.fsManager.resolveFile(source);
		return sftpFile.getContent().getInputStream();
	}

	@Override
	protected void postDownloadCleanup() throws Exception {
		FileSystem fs = sftpFile.getFileSystem();
        fsManager.closeFileSystem(fs);
	}

}
