package app.downloadcreater;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;

import app.download.HTTPDownload;
import app.download.SFTPDownload;
import framework.Download;
import framework.DownloadActions;
import framework.DownloadCreater;

public class Downloads implements DownloadCreater{

	@Override
	public Download createDownload(String src, String target, DownloadActions actions) throws URISyntaxException {
		
		if(src.startsWith("http")) {
			return new HTTPDownload(new URI(src), new File(target), actions);
		}
		if(src.startsWith("sftp")) {
			return new SFTPDownload(new URI(src), new File(target), actions);
		}
		if(src.startsWith("ftp")) {
			return new SFTPDownload(new URI(src), new File(target), actions);
		}
		return null;
	}

}
