package app.manager;

import java.util.concurrent.Executors;

import framework.DownloadCreater;
import framework.DownloadManager;
import framework.DownloadMonitor;

public class FixedParallelismDownloadManager extends DownloadManager{
	
	private int parallelism;

	public FixedParallelismDownloadManager(int parallelism, DownloadMonitor monitor, DownloadCreater downloads) {
		super(Executors.newFixedThreadPool(parallelism), monitor, downloads);
		this.parallelism = parallelism;
	}

	public int getParallelism() {
		return parallelism;
	}

}
