package consoleapp.monitor;

import java.text.MessageFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import framework.Download;
import framework.DownloadActions;
import framework.DownloadMonitor;
import framework.constant.DownloaderConstants;
import framework.constant.StatusCode;

public class ConsoleDownloadMonitor extends DownloadMonitor {

	private Map<String, String> downloadData;
	private Map<Download, Date> activeDownloads;
	private DownloadActions actions;

	public ConsoleDownloadMonitor(long refreshIntervalMillis) {
		super(refreshIntervalMillis);
		downloadData = new ConcurrentHashMap<String, String>();
		activeDownloads = new ConcurrentHashMap<Download, Date>();
		actions = new DownloadActions() {

			@Override
			public void queuedAction(Download src) {
				// Can use this to maintain a collection of all queued downloads
			}
			
			@Override
			public void startAction(Download src) {
				activeDownloads.put(src, new Date());
			}

			@Override
			public void completeAction(Download src) {
				activeDownloads.remove(src);
				downloadData.put(src.getSource().toASCIIString(), MessageFormat
						.format(DownloaderConstants.DOWNLOAD_COMPLETE_MESSAGE,
								src.getTarget().getAbsolutePath()));
			}

			@Override
			public void errorAction(Download src) {
				activeDownloads.remove(src);
				downloadData.put(src.getSource().toASCIIString(), MessageFormat
						.format(DownloaderConstants.DOWNLOAD_ERROR_MESSAGE,
								src.getDownloadStatusMessage(),
								src.getCurrentSize(), src.getTotalSize()));
			}

		};
	}
	
	private Map<String,String> refreshDownloadsStatus() {
		
		HashMap<String,String> downloadDataCopy = new HashMap<String,String>();
		synchronized(downloadData) {
			downloadDataCopy.putAll(downloadData);
			downloadData.clear();
		}
		
		//Get status for the active downloads
		for (Download activeDownload : activeDownloads.keySet()) {
			if (activeDownload.getDownloadStatus() == StatusCode.INPROGRESS) {
				downloadDataCopy.put(activeDownload.getSource().toASCIIString(),
						MessageFormat.format(
								DownloaderConstants.DOWNLOAD_PROGRESS_MESSAGE,
								activeDownload.getCurrentSize(),
								activeDownload.getTotalSize()));
			}
		}
		
		return downloadDataCopy;
	}

	@Override
	public void publishResults() {
		
		Map<String, String> downloadStatus = refreshDownloadsStatus();
		System.out.println(new Date());
		for(String data : downloadStatus.keySet()) {
			String statusLine = downloadStatus.get(data);
			System.out.println();
			System.out.println(data);
			System.out.println(statusLine);
		}
		System.out.println("===================");
	}

	@Override
	public DownloadActions getDownloadActions() {
		return actions;
	}

}
