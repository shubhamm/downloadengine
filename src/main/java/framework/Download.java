package framework;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URI;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import app.download.SFTPDownload;
import framework.constant.DownloaderConstants;
import framework.constant.StatusCode;

public abstract class Download implements Runnable {

	protected URI source;

	protected File targetDir;
	
	protected File target;
	
	protected long totalSize;
	
	protected long currentSize;
	
	protected StatusCode downloadStatus;
	
	protected String downloadStatusMessage;
	
	protected DownloadActions actions;
	
	private static final Logger logger = LoggerFactory.getLogger(Download.class);
	
	public Download(URI source, File targetDir, DownloadActions actions) {
		this.source = source;
		this.targetDir = targetDir;
		this.actions = actions;
		downloadStatus = StatusCode.QUEUED;
	}
	
	public void run() {
		
		try	{
			downloadStatus = StatusCode.INPROGRESS;
			actions.startAction(this);
			download();
			downloadStatus = StatusCode.COMPLETED;
			actions.completeAction(this);
		}
		catch(Exception ex)	{
			downloadStatus = StatusCode.ERRORED;
			downloadStatusMessage = ex.getMessage();
			actions.errorAction(this);
			logger.error("Error while downloading "+source.toASCIIString(), ex);
		}
	}

	protected void download() throws Exception {
		InputStream inputStream = setupDownloadStream();
		totalSize = getTotalSize();
		target = new File(targetDir,getFileName());
		downloadFromStream(inputStream);
		postDownloadCleanup();
	}

	protected abstract String getFileName();

	protected void downloadFromStream(InputStream inputStream) throws Exception {
		if(!targetDir.exists() && !targetDir.mkdirs()) {
			throw new IOException("Unable to create target directory: "+targetDir.getAbsolutePath());
		}
		byte[] buffer = new byte[DownloaderConstants.DEFAULT_DOWNLOAD_BUFFER_SIZE];
		int readBytes = 0;
		File tempFile = File.createTempFile(UUID.randomUUID().toString(), null);
		OutputStream outputStream = new FileOutputStream(tempFile);
		while ((readBytes = inputStream.read(buffer)) != -1) {
			currentSize += readBytes;
			outputStream.write(buffer, 0, readBytes);
		}
		outputStream.close();
		Files.move(tempFile.toPath(), target.toPath(), StandardCopyOption.REPLACE_EXISTING);
		inputStream.close();
	}
	
	public abstract long getTotalSize();

	protected abstract InputStream setupDownloadStream() throws Exception;

	protected abstract void postDownloadCleanup() throws Exception;

	public URI getSource() {
		return source;
	}

	public File getTarget() {
		return new File(target.getAbsolutePath());
	}

	public long getCurrentSize() {
		return currentSize;
	}

	public StatusCode getDownloadStatus() {
		return downloadStatus;
	}

	public String getDownloadStatusMessage() {
		return downloadStatusMessage;
	}

}
