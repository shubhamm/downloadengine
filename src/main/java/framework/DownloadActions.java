package framework;

public interface DownloadActions {
	
	public void queuedAction(Download src);
	
	public void startAction(Download src);
	
	public void completeAction(Download src);
	
	public void errorAction(Download src);

}
