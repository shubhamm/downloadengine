package framework;

public interface DownloadCreater {
	
	Download createDownload(String src, String target, DownloadActions monitor) throws Exception;

}
