package framework;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DownloadManager{
	
	protected ExecutorService threadPool;
	
	protected DownloadMonitor monitor;
	
	protected DownloadCreater downloads;

	protected Thread monitorThread;
	
	private static final Logger logger = LoggerFactory.getLogger(DownloadManager.class);
	
	public DownloadManager(ExecutorService threadPool, DownloadMonitor monitor, DownloadCreater downloads) {
		this.monitor = monitor;
		this.threadPool = threadPool;
		this.downloads = downloads;
		monitorThread = new Thread(monitor);
		monitorThread.start();
	}
	
	public void addDownload(String sourceUrl, String destPath) {
		
		Download download = null;
		try {
			download = downloads.createDownload(sourceUrl, destPath, monitor.getDownloadActions());
		}
		catch(Exception ex) {
			logger.error("Unable to add download "+sourceUrl, ex);
		}
		
		if(download != null) {
			monitor.getDownloadActions().queuedAction(download);
			threadPool.execute(download);
		}
	}
	
	public void shutdown() {
		try {
			threadPool.shutdown();
			while(!threadPool.awaitTermination(1, TimeUnit.MINUTES));
			monitor.shutdown();
		}
		catch(Exception ex) {
			logger.error("Error encountered while shutdown, shutting down abnormally", ex);
			System.exit(-1);
		}
	}
}
