package framework;

public abstract class DownloadMonitor implements Runnable {

	private long refreshIntervalMillis;
	
	private boolean shutdown;

	public abstract void publishResults();
	
	public abstract DownloadActions getDownloadActions();
	
	public DownloadMonitor(long refreshIntervalMillis) {
		this.refreshIntervalMillis = refreshIntervalMillis;
		shutdown = false;
	}
	
	@Override
	public void run() {
		while(!shutdown) {
			publishResults();
			try {
				Thread.sleep(refreshIntervalMillis);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		publishResults();
	}
	
	public void shutdown() {
		shutdown = true;
	}

	public boolean isShutdown() {
		return shutdown;
	}
	
	
}
