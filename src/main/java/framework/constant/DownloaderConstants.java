package framework.constant;

public class DownloaderConstants {
	
	public static final int MONITOR_REFRESH_INTERVAL = 5000;
	public static final int DEFAULT_DOWNLOAD_BUFFER_SIZE = 4096;
	public static final int DEFAULT_DOWNLOAD_PARALLELISM = 8;
	public static final String DOWNLOAD_START_MESSAGE = "Starting download";
	public static final String DOWNLOAD_PROGRESS_MESSAGE = "Downloading progress: {0}/{1}";
	public static final String DOWNLOAD_ERROR_MESSAGE = "Error occoured: {0}, progress: {1}/{2}";
	public static final String DOWNLOAD_COMPLETE_MESSAGE = "Download completed, file downloaded to path: {0}";
}
