package framework.constant;

public enum StatusCode {
	QUEUED, INPROGRESS, COMPLETED, ERRORED
}
