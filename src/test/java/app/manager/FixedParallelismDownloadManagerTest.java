package app.manager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.powermock.api.easymock.PowerMock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import framework.Download;
import framework.DownloadActions;
import framework.DownloadCreater;
import framework.DownloadMonitor;

@RunWith(PowerMockRunner.class)
@PrepareForTest( {FixedParallelismDownloadManagerTest.class} )
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class FixedParallelismDownloadManagerTest {
	
	private static final int PARALLELISM = 4;
	FixedParallelismDownloadManager testable;
	DownloadMonitor mockMonitor;
	DownloadCreater mockDownloadsFactory;
	List<Download> downloads = new ArrayList<>();
	Download downloadMock;
	
	@Before
	public void setup() {
		
		downloadMock = PowerMock.createNiceMock(Download.class);
		
		mockDownloadsFactory = new DownloadCreater() {
			
			@Override
			public Download createDownload(String src, String target,
					DownloadActions monitor) throws Exception {
				return downloadMock;
			}
		};
		
		mockMonitor = new DownloadMonitor(1000) {
			
			@Override
			public void publishResults() {
				
			}
			
			@Override
			public DownloadActions getDownloadActions() {
				return new DownloadActions() {
					
					@Override
					public void startAction(Download src) {
					}
					
					@Override
					public void queuedAction(Download src) {
						downloads.add(src);
					}
					
					@Override
					public void errorAction(Download src) {
					}
					
					@Override
					public void completeAction(Download src) {
					}
				};
			}
		};
		testable = new FixedParallelismDownloadManager(PARALLELISM, mockMonitor, mockDownloadsFactory);
	}
	
	
	@Test
	public void testAddDownload() {
		
		testable.addDownload("ABC", "QWE");
		assertTrue(downloads.contains(downloadMock));
	}
	
	@Test
	public void testShutdown() {
		
		testable.shutdown();
		assertTrue(mockMonitor.isShutdown());
	}
	
	@Test
	public void testGetParallelism() {
		assertEquals(PARALLELISM,testable.getParallelism());
	}

}
