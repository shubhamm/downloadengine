package consoleapp.main;

import app.downloadcreater.Downloads;
import app.manager.FixedParallelismDownloadManager;
import consoleapp.monitor.ConsoleDownloadMonitor;
import framework.DownloadManager;

public class ConsoleDownloaderSample {
	
	public static void main(String[] args) {
		ConsoleDownloadMonitor monitor = new ConsoleDownloadMonitor(5000);
		DownloadManager manager = new FixedParallelismDownloadManager(8, monitor, new Downloads());
		manager.addDownload("http://www.7-zip.org/a/7z1604-x64.exe", "D:/sftp");
		manager.addDownload("sftp://shubhamme:passwd@10.66.43.60/tmp/loremipsum.txt", "D:/sftp");
		manager.addDownload("ftp://user:pass@localhost:2121/jdk-8u65-windows-x64.exe", "D:/sftp");
		manager.shutdown(); 
	}
}
